package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Tasks{
	public WebDriver driver;
	By addTasks=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div[1]/div[2]/button");
	
	public Tasks(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement content() {
		return driver.findElement(addTasks);
		
	}
}
