package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Reporting{
	public WebDriver driver;
	By reporting=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div/div[1]/div/div/div/div/div[1]");
	
	public Reporting(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement content() {
		return driver.findElement(reporting);
		
	}
}
