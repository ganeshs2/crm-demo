package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Admin {
	public WebDriver driver;
	By addUser=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div/div/div[3]/div/div[2]/div[1]/div[2]/div/div/div/div/button[2]");
	
	public Admin(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}
	public WebElement content() {
		return driver.findElement(addUser);
		
	}
}
