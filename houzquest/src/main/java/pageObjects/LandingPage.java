package pageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LandingPage {

	
	public WebDriver driver;
	
	By signinWithEmail =By.xpath("//*[@id=\'root\']/div/div[2]/div/div[2]/div[2]/span");
	By nav_Mail = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[4]/a");
	By nav_Text = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[6]/a");
	By nav_Tasks = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[8]");
	By nav_Calender = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[10]/a");
	By nav_Admin = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[12]/a");
	By nav_Reporting = By.xpath("//*[@id=\"root\"]/div/section/section/header/div/div/div[1]/div[2]/div/ul/li[14]/a");
	By addUser = By.xpath("/html/body/div[1]/div/section/section/div/div/div[2]/div/div[4]/button");
	By tableRow = By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div[2]/div/div/div/div/div/div/div/div/div[2]/table");
	By table=By.xpath("//*[@id=\"root\"]/div/section/section/main/div/div/div[2]/div/div/div/div/div/div/div/div/div[2]/table");
	By tableSecondCol=By.cssSelector("td:nth-child(3)");
	
	
	
	
	
	
	
	public LandingPage(WebDriver driver) {
		// TODO Auto-generated constructor stub
		
		this.driver=driver;
		
	}




	public LoginPage getLogin()
	{
		 driver.findElement(signinWithEmail).click();
		 LoginPage lp=new LoginPage(driver);
		 return lp;		 
	}
	public WebElement Mail()
	{
		return driver.findElement(nav_Mail);
		 
	}
	public WebElement Text()
	{
		return driver.findElement(nav_Text);
		 
	}
	public WebElement Tasks()
	{
		return driver.findElement(nav_Tasks);
		 
	}
	public WebElement Calender()
	{
		return driver.findElement(nav_Calender);
		 
	}
	public WebElement Admin()
	{
		return driver.findElement(nav_Admin);
		 
	}
	public WebElement Reporting()
	{
		
		return driver.findElement(nav_Reporting);
		 
	}
	
	public WebElement addUser()
	{
		return driver.findElement(addUser);
		 
	}
	public WebElement table()
	{
		return driver.findElement(table);
	}
	public List<WebElement> tableSecondCol()
	{
		return driver.findElements(tableSecondCol);
	}
	
	
}
