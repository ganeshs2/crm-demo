package CRM.houzquest;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.awt.Window;
import java.io.IOException;
import java.util.NoSuchElementException;


import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.mongodb.operation.CurrentOpOperation;

import pageObjects.AddUser;
import pageObjects.Admin;
import pageObjects.Calender;
import pageObjects.LandingPage;
import pageObjects.Mail;
import pageObjects.Reporting;
import pageObjects.Tasks;
import pageObjects.Text;
import resources.Read_XLS;
import resources.SuiteUtility;
import resources.base;

public class AddLead extends base{
	Read_XLS FilePath = null;	
	String TestCaseName = null;
	public WebDriver driver;
//	 public static Logger log =LogManager.getLogger(base.class.getName());
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver =initializeDriver();
		 
		 init();	
			FilePath = TestCaseListExcelOne;
			//System.out.println("FilePath Is : "+FilePath);
			TestCaseName = this.getClass().getSimpleName();	
			//System.out.println("TestCaseName Is : "+TestCaseName);

	}	
	 @Test
	public void Login() throws InterruptedException {
			 //WebDriver driver =null;
			 //Login lg = new Login();
			 login();
	}

	@Test(dataProvider="SuiteOneCaseOneData")
	public void addUser(String firstName, String lastName, String email, String mobile, String city, String price) throws InterruptedException, ElementNotInteractableException, NoSuchElementException, TimeoutException{
		AddUser au = new AddUser(driver);
		LandingPage lp = new LandingPage(driver);
		driver.navigate().refresh();
		lp.addUser().click();
		au.firstName().sendKeys(firstName);
		au.lastName().sendKeys(lastName);
		au.emailAddress().sendKeys(email);
		au.phoneNumber().sendKeys(mobile);
		Thread.sleep(5000);
		au.sourceName().click();
		Thread.sleep(3000);
		au.source().click();
		au.buyer().click();
		au.seller().click();
		Thread.sleep(3000);
		au.city().click();
		au.citySelection().sendKeys(city + Keys.ENTER);
		au.price().sendKeys(price);
		au.bedroom().click();
		au.bedroomSelection().click();
		au.bathroom().click();
		au.bathroomSelection().click();
		au.add().click();

		
			WebDriverWait wait = new WebDriverWait(driver,5);
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@class='ant-notification-notice-message']")));
			int s = driver.findElements(By.xpath("//div[@class='ant-notification-notice-message']")).size();

		
//			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[14]/div/span/div[2]/div/div/div[1]")));
			
			Thread.sleep(1000);
			for(int i = 1;i <= s; i++) {
			System.out.println(driver.findElement(By.xpath("(//div[@class='ant-notification-notice-message'])["+i+"]")).getText());
				
			}
		
			windowHandler(driver);
			int w = driver.getWindowHandles().size();
//			System.out.println(s);
			
			if(w>1) {
				driver.switchTo().window(srtingArray.get(1));
				Thread.sleep(6000);
				try {
					System.out.println(srtingArray.get(1));
					if(driver.findElement(By.xpath("//*[@id=\"root\"]/div/section/section/div/div/div[2]/div")).isDisplayed()) {
						System.out.println("Lead added succesfully");
					}else {
						System.out.println("test case failed.");
					}
				}catch (TimeoutException e1) {
					System.out.println("exception.");
				}
			}

		    
		    
		    
		
		
		//au.cancel().click();
		

	}
	@DataProvider
	public Object[][] SuiteOneCaseOneData(){
		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteOneCaseOne data Sheet.
		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
	}
	@AfterTest
	public void teardown()
	{
		
		//driver.close();
	
		
	}
}
