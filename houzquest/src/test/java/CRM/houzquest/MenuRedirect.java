package CRM.houzquest;

import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;
import org.testng.AssertJUnit;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.Admin;
import pageObjects.Calender;
import pageObjects.LandingPage;
import pageObjects.Mail;
import pageObjects.Reporting;
import pageObjects.Tasks;
import pageObjects.Text;
import resources.Read_XLS;
import resources.SuiteUtility;
import resources.base;

public class MenuRedirect extends base {
//	Read_XLS FilePath = null;	
//	String TestCaseName = null;
	public WebDriver driver;
//	 public static Logger log =LogManager.getLogger(base.class.getName());
	@BeforeTest
	public void initialize() throws IOException
	{
	
		 driver =initializeDriver();
//		 log.info("Driver is initialized");
	}	
//	@BeforeTest
//	public void checkCaseToRun() throws IOException{
//		//Called init() function from SuiteBase class to Initialize .xls Files
//		init();	
//		FilePath = TestCaseListExcelOne;
//		System.out.println("FilePath Is : "+FilePath);
//		TestCaseName = this.getClass().getSimpleName();	
//		System.out.println("TestCaseName Is : "+TestCaseName);
//	}
	 @Test
	public void Login() throws InterruptedException {
			 //WebDriver driver =null;
			 //Login lg = new Login();
			 login();
	}

	@Test
	public void navigationTesting() throws InterruptedException {
		LandingPage  lp = new LandingPage(driver);
		//Login lo = new Login();
		Mail m = new Mail(driver);
		Text t = new Text(driver);
		Tasks ts = new Tasks(driver);
		Reporting r = new Reporting(driver);
		Calender c = new Calender(driver);
		Admin a = new Admin(driver);
		Thread.sleep(5000);
		lp.Tasks().click();
		AssertJUnit.assertTrue(ts.content().isDisplayed());
		System.out.println("Tasks page Verified");
		
		lp.Mail().click();
		AssertJUnit.assertTrue(m.content().isDisplayed());
		System.out.println("Mail page Verified");
		lp.Text().click();
		AssertJUnit.assertTrue(t.content().isDisplayed());
		System.out.println("Text page Verified");
		lp.Calender().click();
		AssertJUnit.assertTrue(c.content().isDisplayed());
		System.out.println("Calender page Verified");
		lp.Admin().click();
		AssertJUnit.assertTrue(a.content().isDisplayed());
		System.out.println("Admin page Verified");
		lp.Reporting().click();
		AssertJUnit.assertTrue(r.content().isDisplayed());
		System.out.println("Reporting page Verified");

		//This data provider method will return 4 column's data one by one In every Iteration.
		
		
	}
//	@DataProvider
//	public Object[][] SuiteOneCaseOneData(){
//		//To retrieve data from Data 1 Column,Data 2 Column,Data 3 Column and Expected Result column of SuiteOneCaseOne data Sheet.
//		//Last two columns (DataToRun and Pass/Fail/Skip) are Ignored programatically when reading test data.
//		return SuiteUtility.GetTestDataUtility(FilePath, TestCaseName);
//	}
	
	@AfterTest
	public void teardown()
	{
		
		//driver.close();
	
		
	}
}
